package com.imamfarisi.sample;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;

import com.imamfarisi.selenium.core.BrowserBuilder;
import com.imamfarisi.selenium.core.EngineMapper;
import com.imamfarisi.selenium.core.util.ExcelUtils;

public class App {

	public static void main(String[] args) {
		new App().start();
	}

	private void start() {
		final String fileExcel = "PATH_TO/data.xls"; // change path
		final String browserDriver = "PATH_TO/chromedriver.exe"; // change path

		// setup xls
		ExcelUtils element = new ExcelUtils(fileExcel, "element").initFile();
		ExcelUtils data = new ExcelUtils(fileExcel, "data").initFile();

		// setup browser & engine mapper
		WebDriver b = new BrowserBuilder().setBrowser(BrowserType.CHROME, browserDriver).build();
		EngineMapper em = new EngineMapper(b);

		for (int dta = 1; dta <= data.getRowCountInSheet(); dta++) {
			try {
				for (int row = 0; row < element.getRowCountInSheet(); row++) {
					String action = element.getCellData(row + 1, 3);
					if (action.contains(",")) {
						String[] dataCells = action.split(",");
						for (String d : dataCells) {
							em.mapElement(element.getCellData(row + 1, 1), element.getCellData(row + 1, 2), d,
									data.getCellData(dta, row));
						}
					} else
						em.mapElement(element.getCellData(row + 1, 1), element.getCellData(row + 1, 2),
								element.getCellData(row + 1, 3), data.getCellData(dta, row));
				}

				data.setCellValue(dta, data.getColCountInSheet(dta),
						LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), fileExcel);
				data.setCellValue(dta, data.getColCountInSheet(dta), "PASS", fileExcel);
				data.setCellValue(dta, data.getColCountInSheet(dta), "-", fileExcel);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					data.setCellValue(dta, data.getColCountInSheet(dta),
							LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), fileExcel);
					data.setCellValue(dta, data.getColCountInSheet(dta), "FAILED", fileExcel);
					data.setCellValue(dta, data.getColCountInSheet(dta), "Because " + e.getMessage(), fileExcel);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}

		b.quit();
	}
}
