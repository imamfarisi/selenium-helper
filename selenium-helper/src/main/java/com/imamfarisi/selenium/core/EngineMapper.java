package com.imamfarisi.selenium.core;

import java.io.File;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.imamfarisi.selenium.core.constant.ActionType;
import com.imamfarisi.selenium.core.constant.ElementByType;

/**
 * 
 * @author imam farisi
 *
 */

public class EngineMapper {

	private WebDriver wd;
	private Boolean setScroll = true;
	private Integer numScroll = 70;
	private Duration duration;

	public EngineMapper(WebDriver wd) {
		this.wd = wd;
	}

	/**
	 * Default scroll on every action is true and 70 scroll bottom
	 * 
	 * @param setScroll to false for disabled scroll
	 * @param numScroll scroll number
	 */
	public EngineMapper setScrollOnEveryAction(Boolean setScroll, Integer numScroll) {
		this.setScroll = setScroll;
		this.numScroll = numScroll;
		return this;
	}

	/**
	 * Set timeout on every actions
	 * 
	 * @param duration in second, ex for 1 second : Duration.ofSeconds(1)
	 * @return
	 */
	public EngineMapper setTimeoutOnEveryAction(Duration duration) {
		this.duration = duration;
		return this;
	}

	public WebElement mapElement(String element, String elementBy, String action, String... data) {
		if (checkActionBrowser(action, data.length > 0 ? data[0] : null))
			return null;

		WebElement we = wd.findElement(mapElementBy(element, elementBy));
		action(we, element, action, data.length > 0 ? data[0] : null);
		return we;
	}

	private By mapElementBy(String element, String elementBy) {
		if (elementBy.equalsIgnoreCase(ElementByType.ID.name()))
			return By.id(element);

		if (elementBy.equalsIgnoreCase(ElementByType.NAME.name()))
			return By.name(element);

		if (elementBy.equalsIgnoreCase(ElementByType.XPATH.name()))
			return By.xpath(element);

		if (elementBy.equalsIgnoreCase(ElementByType.CLASS.name()))
			return By.className(element);

		if (elementBy.equalsIgnoreCase(ElementByType.CSS.name()))
			return By.cssSelector(element);

		return null;
	}

	private void action(WebElement we, String element, String action, String data) {
		JavascriptExecutor js = (JavascriptExecutor) wd;

		if (action.equalsIgnoreCase(ActionType.SET.name())) {
			if (data != null)
				we.sendKeys(data);
			else
				we.clear();
		} else if (action.equalsIgnoreCase(ActionType.CLICK.name())) {
			if (element.toLowerCase().contains("radio") || element.toLowerCase().contains("checkbox")) {
				js.executeScript("arguments[0].click();", we);
			} else
				we.click();
		} else if (action.equalsIgnoreCase(ActionType.ENTER.name())) {
			we.sendKeys(Keys.ENTER);
		} else if (action.equalsIgnoreCase(ActionType.CTRL_A.name())) {
			we.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		} else if (action.equalsIgnoreCase(ActionType.HOVER.name())) {
			new Actions(wd).moveToElement(we).perform();
			try {
				Thread.sleep(Duration.ofSeconds(1).toMillis());
			} catch (Exception e) {
			}
		} else if (action.equalsIgnoreCase(ActionType.ESC.name())) {
			we.sendKeys(Keys.ESCAPE);
		} else if (action.equalsIgnoreCase(ActionType.CLEAR.name())) {
			we.clear();
		} else if (action.startsWith(ActionType.WAIT.name().toLowerCase())) {
			waitAction(action);
		}

		if (setScroll)
			js.executeScript("window.scrollBy(0," + numScroll + ")");

		if (duration != null)
			try {
				Thread.sleep(duration.toMillis());
			} catch (Exception e) {
			}

	}

	private void waitAction(String action) {
		action = action.replaceFirst("wait", "");
		action = action.replaceFirst("|", "");
		action = action.substring(0, action.length() - 1);

		String timeStr = action.substring(1, action.indexOf("|"));
		String element = action.substring(action.indexOf("|") + 1, action.lastIndexOf("|"));
		String elementBy = action.substring(action.lastIndexOf("|") + 1, action.length());

		WebDriverWait wait = new WebDriverWait(wd, Integer.valueOf(timeStr));
		wait.until(ExpectedConditions.visibilityOfElementLocated(mapElementBy(element, elementBy)));
	}

	public void takeSnapShot(String path, String name) throws Exception {
		TakesScreenshot scrShot = ((TakesScreenshot) wd);

		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		File DestFile = new File(path + "/" + name + ".jpg");

		FileUtils.copyFile(SrcFile, DestFile);
	}

	private Boolean checkActionBrowser(String action, String data) {
		if (action.equalsIgnoreCase(ActionType.OPEN.name()) || action.equalsIgnoreCase(ActionType.BACK.name())
				|| action.equalsIgnoreCase(ActionType.FORWARD.name())
				|| action.equalsIgnoreCase(ActionType.REFRESH.name())) {

			if (action.equalsIgnoreCase(ActionType.OPEN.name())) {
				wd.navigate().to(data);
			} else if (action.equalsIgnoreCase(ActionType.BACK.name())) {
				wd.navigate().back();
			} else if (action.equalsIgnoreCase(ActionType.FORWARD.name())) {
				wd.navigate().forward();
			} else if (action.equalsIgnoreCase(ActionType.REFRESH.name())) {
				wd.navigate().refresh();
			}
			return true;
		}
		return false;
	}

}
