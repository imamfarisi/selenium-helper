package com.imamfarisi.selenium.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelUtils {
	private HSSFWorkbook workbook;
	private HSSFSheet sheet;
	private HSSFCell cell;
	private String absolutePathExcel;
	private String sheetName;

	public ExcelUtils(String absolutePathExcel, String sheetName) {
		this.absolutePathExcel = absolutePathExcel;
		this.sheetName = sheetName;
	}

	public ExcelUtils initFile() {
		try {
			File file = new File(absolutePathExcel);
			FileInputStream inputStream = new FileInputStream(file);
			workbook = new HSSFWorkbook(inputStream);
			sheet = workbook.getSheet(sheetName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public String getAbsolutePathExcel() {
		return absolutePathExcel;
	}

	public String getCellData(int rowNumber, int cellNumber) {
		try {
			cell = sheet.getRow(rowNumber).getCell(cellNumber);
			return cell.getStringCellValue();
		} catch (Exception e) {
			return null;
		}
	}

	public Integer getCellColumnByHeaderName(String headerName) {
		for (int dta = 0; dta <= sheet.getRow(0).getLastCellNum(); dta++) {
			if (sheet.getRow(0).getCell(dta).getStringCellValue().equalsIgnoreCase(headerName)) {
				return dta;
			}
		}

		return null;
	}

	public int getRowCountInSheet() {
		return sheet.getLastRowNum() - sheet.getFirstRowNum();
	}

	public int getColCountInSheet(int row) {
		return sheet.getRow(row).getLastCellNum();
	}

	public void setCellValue(int rowNum, int cellNum, String cellValue, String excelFilePath) throws IOException {
		sheet.getRow(rowNum).createCell(cellNum).setCellValue(cellValue);

		FileOutputStream outputStream = new FileOutputStream(excelFilePath);
		workbook.write(outputStream);
	}
}
