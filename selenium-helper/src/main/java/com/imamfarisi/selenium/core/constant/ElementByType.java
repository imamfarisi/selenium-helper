package com.imamfarisi.selenium.core.constant;

/**
 * 
 * @author imam farisi
 *
 */

public enum ElementByType {
	ID, NAME, XPATH, CLASS, CSS
}
