package com.imamfarisi.selenium.core;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;

/**
 * 
 * @author imam farisi
 *
 */

public class BrowserBuilder {

	private String browserType = BrowserType.FIREFOX;
	private String absolutePathDriver = "";
	private Long timeOut;
	private Boolean isMaximize = true;

	/**
	 * First, you need to download your browser driver and then set to
	 * absolutePathDriver param
	 * 
	 * @param browserType,        ex : BrowserType.FIREFOX
	 * @param absolutePathDriver, ex : c:/WebDriver/bin/geckodriver.exe
	 */
	public BrowserBuilder setBrowser(String browserType, String absolutePathDriver) {
		this.browserType = browserType;
		this.absolutePathDriver = absolutePathDriver;
		return this;
	}

	/**
	 * @param timeout in seconds, ex for 5 seconds just using Duration java 8 :
	 *                Duration.ofSeconds(5).getSeconds()
	 */
	public BrowserBuilder setTimeOut(Long timeout) {
		this.timeOut = timeout;
		return this;
	}

	public BrowserBuilder setMaximize(boolean isMaximize) {
		this.isMaximize = isMaximize;
		return this;
	}

	public WebDriver build() {
		WebDriver wd = null;
		if (this.browserType.equals(BrowserType.CHROME)) {
			System.setProperty("webdriver.chrome.driver", this.absolutePathDriver);
			wd = new ChromeDriver();
		} else if (this.browserType.equals(BrowserType.FIREFOX)) {
			System.setProperty("webdriver.gecko.driver", this.absolutePathDriver);
			wd = new FirefoxDriver();
		} else if (this.browserType.equals(BrowserType.EDGE)) {
			System.setProperty("webdriver.edge.driver", this.absolutePathDriver);
			wd = new FirefoxDriver();
		} else if (this.browserType.equals(BrowserType.IE) || this.browserType.equals(BrowserType.IEXPLORE)) {
			System.setProperty("webdriver.ie.driver", this.absolutePathDriver);
			wd = new FirefoxDriver();
		}

		if (timeOut != null)
			wd.manage().timeouts().implicitlyWait(this.timeOut, TimeUnit.SECONDS);
		if (isMaximize)
			wd.manage().window().maximize();

		return wd;
	}
}
