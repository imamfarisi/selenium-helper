package com.imamfarisi.selenium.core.constant;

/**
 * 
 * @author imam farisi
 *
 */

public enum ActionType {
	SET, CLICK, ENTER, CTRL_A, CLEAR, WAIT, ESC, HOVER, OPEN, REFRESH, BACK, FORWARD
}
